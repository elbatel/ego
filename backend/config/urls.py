from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import permissions



urlpatterns = [
    path('admin/', admin.site.urls),
    
    path("api/accounts/",include("accounts.urls")),
    path("api/service/",include("service.urls")),
    path("api/contact/",include("contact.urls")),
    path("api/schema/",include("docs.urls")),
    
] + static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)

# Todo ==> 
# from django.urls import re_path
# from django.views.generic import TemplateView
# urlpatterns += [re_path(r'^,*', TemplateView.as_view(template_name="index.html"))]
