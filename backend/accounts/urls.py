from django.contrib import admin
from django.urls import path, include
from .routers import router

# Todo to delete 
from .views import RedirectSocial


app_name = "accounts"

urlpatterns = [
    path("", include(router.urls)),
    
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),
    
    # Todo to delete 
    path('profile/', RedirectSocial.as_view())
]