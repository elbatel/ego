from django.dispatch import receiver
from djoser.signals import user_activated



@receiver(user_activated)
def user_confirmed_email(user, request):
    pass