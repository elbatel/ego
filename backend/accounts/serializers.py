from djoser.serializers import UserCreateSerializer
from django.contrib.auth import get_user_model
from rest_framework import serializers
# User == Custom User model
User = get_user_model()

class UserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model   =   User
        fields  =   ('id', 'first_name', 'last_name' ,'email', 'password', 'profile_photo', 'about_you', 'phone', 'receive_email', 'is_available')
        
        
