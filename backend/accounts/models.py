from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from .managers import UserManager

def profile_img_dir(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return f"user_{instance.id}/{filename}"

class User(AbstractUser):
    username            =   None
    email               =   models.EmailField(_('Email address'), unique=True)
    profile_photo       =   models.ImageField(upload_to=profile_img_dir, null=True, blank=True)
    about_you           =   models.TextField(max_length=50000, blank=True, null=True)
    phone               =   PhoneNumberField(_('Phone number'), blank=True, null=True)
    receive_email       =   models.BooleanField(_('Receive email'), default=True, db_index=True)
    confirmation_email  =   models.BooleanField(default=False)
    is_available        =   models.BooleanField(_('Is Available'), default=True)
    

    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email
    
    
  
    
# TODO
    #   Favorites
    #   Contact Us
    #   Complaints User 
    #   Desactiv User