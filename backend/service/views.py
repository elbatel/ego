
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from .permissions import IsOwnerOrReadOnly
from .serializers import ServiceAdSerializer, CategorySerializer
from .models import Service_Ad, Category


    
class ServiceAdViewSet(viewsets.ModelViewSet):
    serializer_class    = ServiceAdSerializer
    queryset            = Service_Ad.objects.all()
    lookup_field        = 'slug'
    permission_classes  =   (AllowAny, ) # Todo ==> delete permission_classes
    # def get_permissions(self):

    #     if self.action == 'partial_update' or self.action == 'update' :
    #         permission_classes = [IsAuthenticated, IsOwnerOrReadOnly] 
    #     else:
    #         permission_classes = [IsAdminUser]
    #     return [permission() for permission in permission_classes]
    
class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class    =   CategorySerializer
    queryset            =   Category.objects.all()
    permission_classes  =   (AllowAny, )  # Todo ==> Modify the permissions to the "IsAdmin"
