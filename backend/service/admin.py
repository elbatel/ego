from django.contrib import admin
from .models import Category, Tag, Service_Ad, Image_Service_Ad

admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Service_Ad)
admin.site.register(Image_Service_Ad)