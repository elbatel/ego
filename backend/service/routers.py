from .views import ServiceAdViewSet, CategoryViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'service_ad', ServiceAdViewSet, basename='service_ad')
router.register(r'category', CategoryViewSet, basename='category')