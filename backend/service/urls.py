from django.urls import path, include
from .routers import router

app_name = "service"

urlpatterns = [
    path("", include(router.urls)),
]