from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth import get_user_model
# User == Custom User model
User = get_user_model()

class BaseModel(models.Model):
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        
class Tag(models.Model):
    tag_name    = models.CharField(max_length=100, unique=True)
    slug        = AutoSlugField(populate_from='tag_name')
    
    def __str__(self):
        return self.tag_name

class Category(BaseModel):
    name            = models.CharField(max_length=100)
    parent_category = models.ForeignKey('self',limit_choices_to={"parent_category__isnull": True} ,related_name="subcategories" ,blank=True, null=True, on_delete=models.CASCADE)
    slug            = AutoSlugField(populate_from='name')
    image_category  = models.ImageField(upload_to='categories/', blank=True, null=True)
    
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ['name']
        
    def __str__(self):
        return self.name

class Service_Ad(BaseModel):
    user            = models.ForeignKey(User, on_delete=models.CASCADE )     
    title           = models.CharField(max_length=200, db_index=True)                                                            #SI
    slug            = AutoSlugField(populate_from='title', unique=True)
    description     = models.TextField(max_length=50000, blank=True, null=True)  
    price           = models.DecimalField(max_digits=12, decimal_places=0, default=0)
    status          = models.IntegerField(default=1)
    email           = models.CharField(max_length=100, blank=True, null=True)                                                   #SI
    phone           = models.CharField(max_length=100, blank=True, null=True)   
    website         = models.CharField(max_length=200, blank=True ,null=True)   
    tags            = models.ManyToManyField(Tag ,blank=True,related_name="tags_service_ad")
    service_image   = models.ImageField(upload_to='service_ad-%Y-%m', blank=True, null=True) 
    category        = models.ForeignKey(Category, null=True, blank=True, on_delete=models.SET_NULL)
    
    # favorit
    # district
    class Meta:
        verbose_name = "Service Ad"
        verbose_name_plural = "Service Ads"
        ordering = ['-created_at']
    
    def __str__(self):                                                                               
        return self.title   
    
class Image_Service_Ad(BaseModel):
    service_ad  = models.ForeignKey(Service_Ad, default=None, related_name='images_service_ad', on_delete=models.CASCADE)
    image       = models.ImageField(upload_to='service_ad-%Y-%m', blank=True)

    def __str__(self):
        return f'Image for : {self.service_ad.title}'



# TODO
    # district
    # recommended_service_ad
    # Complaint_service_ad