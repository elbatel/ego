from rest_framework import serializers
from django.contrib.auth import get_user_model

from accounts.serializers import UserCreateSerializer
from .models import Tag, Service_Ad, Category, Image_Service_Ad

# User == Custom User model
User = get_user_model()


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'slug', 'image_category', 'parent_category', 'subcategories')

    def get_fields(self):
        fields = super(CategorySerializer, self).get_fields()
        fields['subcategories'] = CategorySerializer(many=True)
        return fields
        
        
class ImageServiceAdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image_Service_Ad
        fields = "__all__"
              
           
class ServiceAdSerializer(serializers.ModelSerializer):
    tags        = TagSerializer(many=True, required=False, read_only=True)
    user        = UserCreateSerializer(required=False, read_only=True)
    category    = CategorySerializer(required=False, read_only=True)
    image_all   = ImageServiceAdSerializer(source='images_service_ad', many=True, required=False, read_only=True)
    serializers.ImageField(use_url=True, required=False, allow_null=True)
    url         = serializers.HyperlinkedIdentityField(view_name="service:service_ad-detail", lookup_field = 'slug')

    class Meta:
        model           = Service_Ad
        fields          = '__all__'
        lookup_field    = 'slug'
        # extra_kwargs = {'url': {'lookup_field': 'slug'}}
        

        
