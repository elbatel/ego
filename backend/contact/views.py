from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.mail import send_mail
from django.conf import settings

from .models import Contact

class ContactCreateView(APIView):
    permission_classes  =   (permissions.AllowAny, )
    
    def post(self, request, format=None):
        data    =   self.request.data
        try:
            send_mail(data['subject'],
                      f'Name : {data["name"]} \nEmail : {data["email"]} \n\n\Message : \n data["message"]',
                      f'{settings.EMAIL_HOST_USER}',
                      [f'{settings.EMAIL_CONTACT}'],
                      fail_silently=False,
                      )
            
            contact = Contact(name=data["name"], email=data["email"], message=data["message"])
            contact.save()
            return Response({'success':'Message sent succesfully'})
        
        except:
            return Response({'error':'Message failed send'})