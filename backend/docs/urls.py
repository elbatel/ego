from django.urls import path, include
from rest_framework.permissions import AllowAny
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from django.conf.urls import url

schema_view = get_schema_view(
    openapi.Info(
        title="Ego API",
        default_version='v1',
        description="EGO",
        terms_of_service="https://ego.pe/",
        contact=openapi.Contact(email="contact@ego.com"),
        license=openapi.License(name="BSD License"),
    ),
        public=True,
        permission_classes=(AllowAny,), # Todo ==> Modify the permissions to the "IsAdmin"
)


urlpatterns = [    
    url(r'^docs(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    
]